In order to exec the Liquibase migrations, just type the following command with proper variables:
> `mvn liquibase:update -Pcustom -Dcustom.ip=<db-host> -Dcustom.port=<db-port> -Dcustom.db=<db-name> -Dcustom.username=<db-user> -Dcustom.password=<db-password>`

Or by profile

> `mvn liquibase:update -Plbonaldo`
