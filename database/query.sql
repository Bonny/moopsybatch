
select 
	id_aquila,
	count(*) as nr_voli
from
	voli_aquila
where 
	anno = EXTRACT(YEAR FROM CURRENT_DATE)
	-- da capire cosa si intende per notte e in tal caso aggiungere 
	-- dei vincoli sulle colonne orario_*
group by id_aquila
    