package it.moopsybatch;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String args[]) {

        int ex = 0;

        try {

            if (args == null || args.length < 3) {
                throw new IllegalArgumentException("Argomenti non validi");
            }

            System.out.println(String.format("driver: %s, url: %s, mese: %s", args[0], args[1], args[2]));

            final Connection c = getConn(args[0], args[1]);

            // No nullpointer perchè i casi di errore vengono gestiti dal try-catch con il rilancio della eccezione
            getVoli(c, Integer.valueOf(args[2])).forEach(System.out::println);

            c.close();

        } catch (Exception e) {
            e.printStackTrace();
            ex = -1;
        } finally {
            System.exit(ex);
        }

    }

    static List<Flight> getVoli(Connection c, int month) throws Exception {
        List<Flight> flights = null;
        PreparedStatement ps = null;

        try {

            // si potrebbero creare un exceptions ad hoc ma dipende dal caso
            if (c == null || c.isClosed()) {
                throw new Exception("Connessione non valida");
            }
            if (month < 1 || month > 12) {
                throw new Exception(String.format("Valore di month [%d] non valido", month));
            }

            // non uso * per evitare di estrarre dati che non servono
            ps = c.prepareStatement("select ID, ID_AQUILA, GIORNO, MESE, ANNO, ORARIO_USCITA, ORARIO_RITORNO from VOLI_AQUILA where mese = ?");
            ps.setInt(1, month);
            ps.setFetchSize(1000);
            /**
             * NB richiesta troppo generica .... si potrebbe prevedere
             * limit/offset parametrici in modo da prevedere una paginazione
             * oppure riempire la lista "paginando" ma gli oggetti sarebbero cmq
             * caricati tutti in memoria
             */
            ResultSet rs = ps.executeQuery();

            flights = new ArrayList<>();
            // è consigliato dichiarare le var fuori dai cicli di eiterazione per rendere la vità più facile al compilatore
            Flight flight = null;
            while (rs.next()) {
                flight = new Flight();
                // non ritornerei al client l'ID pk della tabella.... ma dipende dal caso...
                flight.setId(rs.getInt("ID"));
                flight.setIdAquila(rs.getInt("ID_AQUILA"));
                flight.setGiorno(rs.getInt("GIORNO"));
                flight.setMese(rs.getInt("MESE"));
                flight.setAnno(rs.getInt("ANNO"));
                flight.setOrarioUscita(rs.getInt("ORARIO_USCITA"));
                flight.setOrarioRitorno(rs.getInt("ORARIO_RITORNO"));
                flights.add(flight);
            }

            rs.close();

        } catch (Exception e) {
            throw new Exception("Errore durante l'esecuzione del metodo getVoli", e);
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
        return flights;
    }

    // in un contesto reale non sarebbe dichiarata static ma inserita in un file ad hoc 
    static class Flight {

        int id;
        int idAquila;
        int giorno;
        int mese;
        int anno;
        int orarioUscita;
        int orarioRitorno;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getIdAquila() {
            return idAquila;
        }

        public void setIdAquila(int idAquila) {
            this.idAquila = idAquila;
        }

        public int getGiorno() {
            return giorno;
        }

        public void setGiorno(int giorno) {
            this.giorno = giorno;
        }

        public int getMese() {
            return mese;
        }

        public void setMese(int mese) {
            this.mese = mese;
        }

        public int getAnno() {
            return anno;
        }

        public void setAnno(int anno) {
            this.anno = anno;
        }

        public int getOrarioUscita() {
            return orarioUscita;
        }

        public void setOrarioUscita(int orarioUscita) {
            this.orarioUscita = orarioUscita;
        }

        public int getOrarioRitorno() {
            return orarioRitorno;
        }

        public void setOrarioRitorno(int orarioRitorno) {
            this.orarioRitorno = orarioRitorno;
        }

        @Override
        public String toString() {
            return "Volo{" + "id=" + id + ", idAquila=" + idAquila + ", giorno=" + giorno + ", mese=" + mese + ", anno=" + anno + ", orarioUscita=" + orarioUscita + ", orarioRitorno=" + orarioRitorno + '}';
        }

    }

    static Connection getConn(String driver, String url) throws Exception {
        try {
            Class.forName(driver);
            return DriverManager.getConnection(url);
        } catch (ClassNotFoundException | SQLException e) {
            throw new Exception("Errore durante la connessione al db", e);
        }
    }

}
